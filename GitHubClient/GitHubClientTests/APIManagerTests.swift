//
//  APIManagerTests.swift
//  GitHubClient
//
//  Created by André Gimenez Faria on 02/03/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import GitHubClient

class APIManagerTests: XCTestCase {
    
    // Teste para validar que a constante de url base da API do Github é válida
    func testValidBaseURL() {
        XCTAssertEqual(APIManager.API_BASE_URL, "https://api.github.com/", "invalid base url")
    }
    
    // Teste para validar a chamada da API para retornar os repositórios populares de Java
    func testSearchAPICall() {
        let apiCallExpectation = self.expectationWithDescription("search api call expectation")
        APIManager.getPopularJavaRepositoriesFromPage(1) { (result, repositories)  in
            XCTAssertEqual(result, APICallResult.Success, "api call failed")
            apiCallExpectation.fulfill()
        }
        self.waitForExpectationsWithTimeout(10.0, handler: nil)
    }
    
    // Teste para validar a chamada API para retornar os pull requests de um determinado repositório
    func testGetPullRequestsAPICall() {
        let apiCallExpectation = self.expectationWithDescription("pull requests get api call expectation")
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("valid_repository", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testRepositoryObject = Mapper<Repository>().map(json)
            APIManager.getPullRequestsFromRepository(testRepositoryObject!, completionHandler: { (result, repositories) in
                XCTAssertEqual(result, APICallResult.Success, "api call failed")
                apiCallExpectation.fulfill()
            })
            self.waitForExpectationsWithTimeout(10.0, handler: nil)
        }
        catch {
            XCTFail("test json not found")
        }

    }
    
}
