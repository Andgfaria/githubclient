//
//  RepositoryTests.swift
//  GitHubClient
//
//  Created by André Gimenez Faria on 02/03/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import GitHubClient

class RepositoryTests: XCTestCase {
    
    // Testa o mapeamento bem sucedido de um JSON para um objeto do tipo Repository
    func testValidMapping() {
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("valid_repository", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testRepositoryObject = Mapper<Repository>().map(json)
            XCTAssertEqual(testRepositoryObject?.id, json["id"], "respository id mapping failed")
            XCTAssertEqual(testRepositoryObject?.name, json["name"], "respository name mapping failed")
            XCTAssertEqual(testRepositoryObject?.description, json["description"], "repository description mapping failed")
            XCTAssertEqual(testRepositoryObject?.starsCount, json["stargazers_count"], "repository stars count mapping failed")
            XCTAssertEqual(testRepositoryObject?.forksCount, json["forks_count"], "repository forks count mapping failed")
                XCTAssertEqual(testRepositoryObject?.url, json["html_url"], "url mapping failed")
            XCTAssertNotNil(testRepositoryObject?.author, "repository owner mapping failed")
        }
        catch {
            XCTFail("test json not found")
        }
    }
    
    // Testa o comportamento de um objeto do tipo Repository após um mapeamento com valores inválidos
    func testInvalidMapping() {
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("invalid_repository", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testRepositoryObject = Mapper<Repository>().map(json)
            XCTAssertNil(testRepositoryObject?.id, "invalid id value")
            XCTAssertNil(testRepositoryObject?.name, "invalid name value")
            XCTAssertNil(testRepositoryObject?.author, "invalid author object")
            XCTAssertNil(testRepositoryObject?.description, "invalid description value")
            XCTAssertNil(testRepositoryObject?.starsCount, "invalid stars count value")
            XCTAssertNil(testRepositoryObject?.forksCount, "invalid forks count value")
            XCTAssertNil(testRepositoryObject?.url, "invalid url value")
        }
        catch {
            XCTFail("test json not found")
        }
    }
    
    
}
