//
//  AuthorTests.swift
//  GitHubClient
//
//  Created by André Gimenez Faria on 02/03/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import GitHubClient

class AuthorTests: XCTestCase {
    
    // Testa o mapeamento bem sucedido de um JSON para um objeto do tipo Author
    func testValidMapping() {
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("valid_author", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testAuthorObject = Mapper<Author>().map(json)
            XCTAssertEqual(testAuthorObject?.id, json["id"], "user id mapping failed")
            XCTAssertEqual(testAuthorObject?.name, json["login"], "user name mapping failed")
            XCTAssertEqual(testAuthorObject?.avatarURL, json["avatar_url"], "avatar url mapping failed")
        }
        catch {
            XCTFail("test json not found")
        }
    }
    
    // Testa o comportamento de um objeto do tipo Author após um mapeamento com valores inválidos
    func testInvalidMapping() {
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("invalid_author", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testAuthorObject = Mapper<Author>().map(json)
            XCTAssertNil(testAuthorObject?.id, "invalid id value")
            XCTAssertNil(testAuthorObject?.name, "invalid name value")
            XCTAssertNil(testAuthorObject?.avatarURL, "invalid avatar url value")
        }
        catch {
            XCTFail("test json not found")
        }

    }
    
}
