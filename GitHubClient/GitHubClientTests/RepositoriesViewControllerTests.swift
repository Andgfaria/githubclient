//
//  RepositoriesViewControllerTests.swift
//  GitHubClient
//
//  Created by André Gimenez Faria on 03/03/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import GitHubClient

class RepositoriesViewControllerTests: XCTestCase {
    
    var repositoriesViewController : RepositoriesViewController!
    
    var testRepositories : [Repository]!
    
    override func setUp() {
        super.setUp()
        // Instanciação de um objeto do tipo RepositoriesViewController para testes
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        repositoriesViewController = mainStoryboard.instantiateViewControllerWithIdentifier("repositoriesScreen") as! RepositoriesViewController
        repositoriesViewController.view.layoutIfNeeded()
        // Instanciação de um objeto do tipo Repository para testes, a partir de um arquivo JSON
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("valid_repository", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testRepository = Mapper<Repository>().map(json)!
            testRepositories = [testRepository]
        }
        catch {
            print("assembly of testRepository failed")
        }
    }
    
    // Teste para validar os componentes de interface no Storyboard
    func testUIComponentsExistence() {
        XCTAssertNotNil(repositoriesViewController.tableView, "table view missing")
        XCTAssertNotNil(repositoriesViewController.activityIndicator, "activity indicator missing")
        XCTAssertNotNil(repositoriesViewController.errorMessageLabel, "error message label missing")
        XCTAssertNotNil(repositoriesViewController.tryAgainButton, "try again button missing")
    }
    
    // Teste para validar o estado inicial da View Controller
    func testInitialState() {
        XCTAssertNil(repositoriesViewController.repositories, "repositories array should be initially nil")
        XCTAssertEqual(repositoriesViewController.currentPage, 1, "initial page should be 1")
        XCTAssertTrue(repositoriesViewController.firstLoad, "firstLoad flag should be initially true")
        XCTAssertFalse(repositoriesViewController.fetchingMoreRepositories, "fetchingMoreRepositories flag should be initially false")
        XCTAssertEqual(repositoriesViewController.tableView.numberOfRowsInSection(0), 0, "there should be no rows initially")
    }
    
    // Teste para validar os componentes da repositoryCell
    func testRepositoryCell() {
        let cell = repositoriesViewController.tableView.dequeueReusableCellWithIdentifier("repositoryCell")
        XCTAssertNotNil(cell, "repositoryCell should exist")
        XCTAssertTrue(cell?.viewWithTag(1) is UIImageView, "view with tag 1 should be a UIImageView")
        XCTAssertTrue(cell?.viewWithTag(2) is UILabel, "view with tag 2 should be a UILabel")
        XCTAssertTrue(cell?.viewWithTag(3) is UILabel, "view with tag 3 should be a UILabel")
        XCTAssertTrue(cell?.viewWithTag(4) is UILabel, "view with tag 4 should be a UILabel")
        XCTAssertTrue(cell?.viewWithTag(5) is UILabel, "view with tag 5 should be a UILabel")
        XCTAssertTrue(cell?.viewWithTag(6) is UILabel, "view with tag 6 should be a UILabel")
    }
    
    // Teste para validar os componentes da showMoreRepositoriesCell
    func testShowMoreRepositoriesCell() {
        let cell = repositoriesViewController.tableView.dequeueReusableCellWithIdentifier("showMoreRepositoriesCell")
        XCTAssertNotNil(cell, "showMoreRepositoriesCell should exist")
        XCTAssertTrue(cell?.viewWithTag(1) is UIActivityIndicatorView, "view with tag 1 should be a UIActivityIndicatorView")
    }
    
    // Testes dos métodos da dataSource da UITableView na View Controller
    
    func testNumberOfRows() {
        repositoriesViewController.repositories = testRepositories
        repositoriesViewController.tableView.reloadData()
        XCTAssertEqual(repositoriesViewController.tableView.numberOfRowsInSection(0), 2, "incorrect number of rows")
    }
    
    func testRepositoryCellSetup() {
        repositoriesViewController.repositories = testRepositories
        repositoriesViewController.tableView.reloadData()
        let repositoryCell = repositoriesViewController.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
        XCTAssertEqual((repositoryCell?.viewWithTag(2) as? UILabel)?.text, testRepositories[0].author?.name ?? "-", "author name was incorrectly shown")
        XCTAssertEqual((repositoryCell?.viewWithTag(3) as? UILabel)?.text, testRepositories[0].name ?? "-", "repository name was incorrectly shown")
         XCTAssertEqual((repositoryCell?.viewWithTag(4) as? UILabel)?.text, testRepositories[0].description ?? "-", "repository description was incorrectly shown")
         XCTAssertEqual((repositoryCell?.viewWithTag(5) as? UILabel)?.text,"\(testRepositories![0].starsCount ?? 0)", "repository star count was incorrectly shown")
        XCTAssertEqual((repositoryCell?.viewWithTag(6) as? UILabel)?.text,"\(testRepositories![0].forksCount ?? 0)", "repository fork count was incorrectly shown")
    }
    
}
