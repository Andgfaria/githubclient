//
//  PullRequestsViewControllerTests.swift
//  GitHubClient
//
//  Created by André Gimenez Faria on 04/03/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import GitHubClient

class PullRequestsViewControllerTests: XCTestCase {
    
    var pullRequestsViewController : PullRequestsViewController!
    
    var testPullRequests : [PullRequest]!
    
    override func setUp() {
        super.setUp()
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        // Instanciação de um PullRequestsViewController para testes
        pullRequestsViewController = mainStoryboard.instantiateViewControllerWithIdentifier("pullRequestsScreen") as! PullRequestsViewController
        pullRequestsViewController.view.layoutIfNeeded()
        // Instanciação de um objeto do tipo Pull Request para testes, a partir de um arquivo JSON
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("valid_pull_request", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testPullRequest = Mapper<PullRequest>().map(json)!
            testPullRequests = [testPullRequest]
        }
        catch {
            print("assembly of testRepository failed")
        }
    }
    
    // Validação dos componentes da View Controller
    func testUIComponentsExistence() {
        XCTAssertNotNil(pullRequestsViewController.tableView, "table view missing")
        XCTAssertNotNil(pullRequestsViewController.activityIndicator, "activity indicator missing")
        XCTAssertNotNil(pullRequestsViewController.errorMessageLabel, "error message label missing")
        XCTAssertNotNil(pullRequestsViewController.tryAgainButton, "try again button missing")
    }
    
    // Validação do estado inicial da View Controller
    func testInitialState() {
        XCTAssertNil(pullRequestsViewController.repository, "repository should be initially nil")
        XCTAssertNil(pullRequestsViewController.pullRequests, "pull requests array should be initially nil")
        XCTAssertEqual(pullRequestsViewController.tableView.numberOfRowsInSection(0), 0, "there should be no rows initially")
    }
    
    // Testes dos componentes da pullRequestCell
    func testPullRequestCell() {
        let cell = pullRequestsViewController.tableView.dequeueReusableCellWithIdentifier("pullRequestCell")
        XCTAssertNotNil(cell, "pullRequestCell should exist")
        XCTAssertTrue(cell?.viewWithTag(1) is UIImageView, "view with tag 1 should be a UIImageView")
        XCTAssertTrue(cell?.viewWithTag(2) is UILabel, "view with tag 2 should be a UILabel")
        XCTAssertTrue(cell?.viewWithTag(3) is UILabel, "view with tag 3 should be a UILabel")
        XCTAssertTrue(cell?.viewWithTag(4) is UILabel, "view with tag 4 should be a UILabel")
        XCTAssertTrue(cell?.viewWithTag(5) is UILabel, "view with tag 2 should be a UILabel")
    }
    
    // Testes dos métodos da dataSource da UITableView na View Controller
    
    func testNumberOfRows() {
        pullRequestsViewController.pullRequests = testPullRequests
        pullRequestsViewController.tableView.reloadData()
        XCTAssertEqual(pullRequestsViewController.tableView.numberOfRowsInSection(0), 1, "incorrect number of rows")
    }
    
    func testPullRequestCellSetup() {
        pullRequestsViewController.pullRequests = testPullRequests
        pullRequestsViewController.tableView.reloadData()
        let cell = pullRequestsViewController.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
        XCTAssertEqual((cell?.viewWithTag(2) as? UILabel)?.text, testPullRequests[0].author?.name ?? "-", "")
        XCTAssertEqual((cell?.viewWithTag(3) as? UILabel)?.text, testPullRequests[0].title ?? "-", "")
        XCTAssertEqual((cell?.viewWithTag(4) as? UILabel)?.text, testPullRequests[0].body ?? "-", "")
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        XCTAssertEqual((cell?.viewWithTag(5) as? UILabel)?.text, dateFormatter.stringFromDate(testPullRequests[0].date!) ?? "-", "")
    }
        
}
