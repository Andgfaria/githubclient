//
//  PullRequestTests.swift
//  GitHubClient
//
//  Created by André Gimenez Faria on 03/03/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import GitHubClient

class PullRequestTests: XCTestCase {

    // Testa o mapeamento bem sucedido de um JSON para um objeto do tipo Pull Request
    func testValidMapping() {
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("valid_pull_request", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testPullRequestObject = Mapper<PullRequest>().map(json)
            XCTAssertEqual(testPullRequestObject?.id, json["id"], "id mapping failed")
            XCTAssertEqual(testPullRequestObject?.title, json["title"], "title mapping failed")
            XCTAssertEqual(testPullRequestObject?.body, json["body"], "body mapping failed")
            XCTAssertNotNil(testPullRequestObject?.author, "author mapping failed")
            XCTAssertNotNil(testPullRequestObject?.date, "date mapping failed")
            XCTAssertNotNil(testPullRequestObject?.url, "url mapping failed")
        }
        catch {
            XCTFail("test json not found")
        }
    }
    
    // Testa o comportamento de um objeto do tipo Pull Request após um mapeamento com valores inválidos
    func testInvalidMapping() {
        let filePath = NSBundle(forClass: self.dynamicType).pathForResource("invalid_pull_request", ofType: "json"),
        data = NSData(contentsOfFile: filePath!)
        do {
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
            let testPullRequestObject = Mapper<PullRequest>().map(json)
            XCTAssertNil(testPullRequestObject?.id, "invalid id value")
            XCTAssertNil(testPullRequestObject?.title, "invalid title value")
        }
        catch {
            XCTFail("test json not found")
        }

    }
    
}
