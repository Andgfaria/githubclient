//
//  AboutViewControllerTests.swift
//  GitHubClient
//
//  Created by André Gimenez Faria on 07/03/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import XCTest
@testable import GitHubClient

class AboutViewControllerTests: XCTestCase {
    
    var aboutViewController : AboutViewController!
    
    override func setUp() {
        super.setUp()
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        // Instanciação de um objeto do tipo AboutViewController para testes
        aboutViewController = mainStoryboard.instantiateViewControllerWithIdentifier("aboutScreen") as! AboutViewController
        aboutViewController.view.layoutIfNeeded()
    }
    
    // Testes do componente de UI da tela
    func testUIComponents() {
        XCTAssertEqual(UIImageJPEGRepresentation(aboutViewController.developerImageView.image!, 1) , UIImageJPEGRepresentation(UIImage(named: "me.jpg")!, 1), "developer image is incorrect")
        XCTAssertEqual(aboutViewController.developerImageView.layer.cornerRadius, 64, "wrong developer imageview corner radius")
        XCTAssertTrue(aboutViewController.developerImageView.layer.masksToBounds, "developer image view should mask to bounds")
        XCTAssertFalse(aboutViewController.aboutTextView.text.isEmpty, "about text view text should not be empty")
    }
    
}
