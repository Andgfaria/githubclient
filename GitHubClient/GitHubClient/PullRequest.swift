/* Pull Request - classe criada por André Gimenez Faria em janeiro de 2016
   Modelo da entidade Pull Request
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import ObjectMapper

class PullRequest: Mappable {
    
    var id : Int?
    
    var title : String?
    
    var body : String?
    
    var author : Author?
    
    var date : NSDate?
    
    var url : String?
    
    required init?(_ map: Map) { }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        body <- map["body"]
        author <- map["user"]
        let ISO8601DateTransform = TransformOf<NSDate,String>(fromJSON:{ (value : String?) -> NSDate? in
            if let value = value {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                return dateFormatter.dateFromString(value)
            }
            return nil
            }, toJSON: { (value : NSDate?) -> String? in
                if let value = value {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                    return dateFormatter.stringFromDate(value)
                }
            return nil
        })
        date <- (map["created_at"], ISO8601DateTransform)
        url <- map["html_url"]
    }
}
