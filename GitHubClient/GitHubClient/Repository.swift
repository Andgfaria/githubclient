/* Repository - classe criada por André Gimenez Faria em janeiro de 2016
   Modelo da entidade Repositório
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import ObjectMapper


class Repository: Mappable {
    
    var id : Int?
    
    var name : String?
    
    var author : Author?
    
    var description : String?

    var starsCount : Int?
    
    var forksCount : Int?
    
    var url : String?
    
    required init?(_ map: Map) { }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        author <- map["owner"]
        description <- map["description"]
        starsCount <- map["stargazers_count"]
        forksCount <- map["forks_count"]
        url <- map["html_url"]
    }
}
