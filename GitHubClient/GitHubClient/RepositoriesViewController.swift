/* RepositoriesViewControllers - classe criada por André Gimenez Faria em janeiro de 2016
   Tela inicial do aplicativo contendo a lista de repositórios populares em Java do GitHub
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import SDWebImage

class RepositoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: Variáveis
    
    // Array com os dados para serem exibidos na tabela
    var repositories : [Repository]?
    
    // Flag para garantir que a requisição inicial seja feita automaticamente uma vez só, quando a tela é exibida ao usuário
    var firstLoad = true
    
    // Contador da página de repositórios
    var currentPage = 1
    
    // Flag para evitar concorrência de requests para carregar os repositórios da próxima página
    var fetchingMoreRepositories = false
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // Mensagem para indicar erros de rede
    @IBOutlet var errorMessageLabel: UILabel!
    
    // Botão para refazer a requisição da API após a falha da primeira tentativa
    @IBOutlet var tryAgainButton: UIButton!
    
    //MARK: Ajustes Iniciais da Tela
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 128
        adjustTryAgainButton()
    }
    
    // Método para alterar a aparência do botão de tentar novamente
    private func adjustTryAgainButton() {
        tryAgainButton.layer.cornerRadius = 5.0
        tryAgainButton.layer.masksToBounds = true
        tryAgainButton.addTarget(self, action: Selector("fetchFirstRepositories"), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func viewWillAppear(animated: Bool) {
        if firstLoad {
            firstLoad = false
            // Primeira tentativa do carregamento dos repositórios
            fetchFirstRepositories()
        }
    }
    
    //MARK: Carregamento dos Dados
    
    // Método para carregar a primeira página dos repositórios populares no Java e exibilos na tabela
    func fetchFirstRepositories() {
        activityIndicator.hidden = false
        tableView.hidden = true
        errorMessageLabel.hidden = true
        tryAgainButton.hidden = true
        APIManager.getPopularJavaRepositoriesFromPage(1) { (callResult, repositories) in
            dispatch_async(dispatch_get_main_queue(), {
                self.activityIndicator.hidden = true
                self.fetchingMoreRepositories = false
                if callResult == .Success {
                    self.repositories = repositories
                    self.tableView.reloadData()
                    self.tableView.hidden = false
                }
                else {
                    self.errorMessageLabel.hidden = false
                    self.errorMessageLabel.text = callResult == .JsonError ? "Erro no JSON" : "Erro de Rede"
                    self.tryAgainButton.hidden = false
                }
            })
        }
    }
    
    func fetchMoreRepositories() {
        APIManager.getPopularJavaRepositoriesFromPage(currentPage + 1) { (callResult, repositories) in
            dispatch_async(dispatch_get_main_queue(), {
                self.fetchingMoreRepositories = false
                if callResult == .Success {
                    if repositories?.count > 0 {
                        self.repositories? += repositories!
                        self.tableView.reloadData()
                        self.currentPage += 1
                    }
                    else {
                        let alertController = UIAlertController(title: "", message: "Não foi possível carregar mais repositórios", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    //MARK: Implementação dos protocolos UITableViewDataSource e UITableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories != nil ? repositories!.count + 1 : 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row < repositories!.count ? UITableViewAutomaticDimension : 64
    }
    
    /* Células utilizadas:
       Repository Cell
        .1: UIImageView para exibir o avatar do autor
        .2: UILabel para o nome do autor
        .3: UILabel para o nome do repositório
        .4: UILabel para a descrição do repositório
        .5: UILabel para a contagem de estrelas
        .6: UILabel para a contagem de forks
       Show More Repositories Cell
        .1: UIActivityIndicatorView para mostrar o processamento da requisição de mais repositórios
    */
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = indexPath.row < repositories!.count ? "repositoryCell" : "showMoreRepositoriesCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath)
        if cell.reuseIdentifier == "repositoryCell" {
            if let authorAvatarImageView = cell.viewWithTag(1) as? UIImageView {
                authorAvatarImageView.layer.cornerRadius = 18
                authorAvatarImageView.layer.masksToBounds = true
                if repositories![indexPath.row].author?.avatarURL != nil {
                    authorAvatarImageView.sd_setImageWithURL(NSURL(string: repositories![indexPath.row].author!.avatarURL!), placeholderImage: UIImage(named: "genericProfile.png"))
                }
            }
            if let authorNameLabel = cell.viewWithTag(2) as? UILabel {
                authorNameLabel.text = repositories![indexPath.row].author?.name ?? "-"
            }
            if let repositoryNameLabel = cell.viewWithTag(3) as? UILabel {
                repositoryNameLabel.text = repositories![indexPath.row].name ?? "-"
            }
            if let repositoryDescriptionLabel = cell.viewWithTag(4) as? UILabel {
                repositoryDescriptionLabel.text = repositories![indexPath.row].description ?? "-"
            }
            if let starsCountLabel = cell.viewWithTag(5) as? UILabel {
                starsCountLabel.text = "\(repositories![indexPath.row].starsCount ?? 0)"
            }
            if let forksCountLabel = cell.viewWithTag(6) as? UILabel {
                forksCountLabel.text = "\(repositories![indexPath.row].forksCount ?? 0)"
            }
        }
        else if cell.reuseIdentifier == "showMoreRepositoriesCell" {
            (cell.viewWithTag(1) as? UIActivityIndicatorView)?.startAnimating()
        }
        return cell
    }
    
    // Implementação do scroll infinito
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == repositories!.count {
            if !fetchingMoreRepositories {
                fetchingMoreRepositories = true
                fetchMoreRepositories()
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row < repositories!.count {
            if (UIApplication.sharedApplication().delegate as! AppDelegate).splitViewController.collapsed {
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
    }
    
    //MARK: Outros
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Segue detail para a tela de pull requests do repositório selecionado
        if segue.identifier == "detailSegue" {
            if let pullRequestsViewController = (segue.destinationViewController as? UINavigationController)?.viewControllers[0] as? PullRequestsViewController {
                let selectedIndex = tableView.indexPathForSelectedRow!
                // Atualização do repositório ativo na tela de detalhes
                pullRequestsViewController.repository = repositories![selectedIndex.row]
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Ações para limpar o cache de imagens
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().clearDisk()
    }

}
