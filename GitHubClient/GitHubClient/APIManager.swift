/* APIManager - classe criada por André Gimenez Faria em janeiro de 2016
   Gerenciador da comunicação do aplicativo com a API do GitHub
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import Alamofire
import ObjectMapper

enum APICallResult : Int {
    case Success = 0,
    ServerError,
    JsonError,
    MissingParametersError
}

class APIManager: NSObject {
    
    static let API_BASE_URL = "https://api.github.com/"

    /* Método que faz uma requisição HTTP para a API do Github para obter a lista de repositórios Java mais populares
       Parâmetros de entrada
        -page : número da página para buscar os resultados
        -completionHandler : callback contendo o resultado da chamada da API e um array de objetos do tipo Repository para uma chamada bem sucedida
    */
    static func getPopularJavaRepositoriesFromPage(page : Int, completionHandler: (APICallResult,[Repository]?) -> ()) {
        Alamofire.request(.GET, API_BASE_URL + "search/repositories",
            parameters: ["q" : "language:Java", "sort" : "stars", "page" : page])
            .responseJSON { response in
                if response.result.isSuccess {
                    if let data = response.result.value as? [String : AnyObject] {
                        if let repositoriesData = data["items"] as? [[String : AnyObject]] {
                            // Mapeamento do JSON para objetos do tipo Repository
                            let repositories = repositoriesData.map{ Mapper<Repository>().map($0)! }
                            // Execução do callback com código de sucesso e array de repositórios
                            completionHandler(.Success,repositories)
                        }
                        else {
                            // Erro na composição do JSON
                            completionHandler(.JsonError,nil)
                        }
                    }
                    else {
                        // Erro na composição do JSON
                        completionHandler(.JsonError,nil)
                    }
                }
                else {
                    // Erro de rede
                    completionHandler(.ServerError,nil)
                }
        }
    }
    
    static func getPullRequestsFromRepository(repository : Repository, completionHandler: (APICallResult,[PullRequest]?) -> ()) {
        if repository.author?.name == nil || repository.name == nil {
            return completionHandler(.MissingParametersError,nil)
        }
        let url = (API_BASE_URL + "repos/" + repository.author!.name! + "/" + repository.name! + "/pulls")
            .stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        Alamofire.request(.GET, url!).responseJSON { response in
            if response.result.isSuccess {
                if let data = response.result.value as? [[String : AnyObject]] {
                    let pullRequests = data.map { Mapper<PullRequest>().map($0)! }
                    completionHandler(.Success,pullRequests)
                }
                else {
                    // Erro na composição do JSON
                    completionHandler(.JsonError,nil)
                }
            }
            else {
                // Erro de rede
                completionHandler(.ServerError,nil)
            }

        }
    }
}
