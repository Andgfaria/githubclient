/* Author - classe criada por André Gimenez Faria em janeiro de 2016
   Modelo da entidade Autor
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import ObjectMapper

class Author: Mappable {
    
    // Id de usuário do GitHub
    var id : Int?
    
    // Nome de usuário do GitHub
    var name : String?
    
    // URL da imagem de perfil do usuário
    var avatarURL : String?
    
    required init?(_ map: Map) { }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["login"]
        avatarURL <- map["avatar_url"]
    }
}
