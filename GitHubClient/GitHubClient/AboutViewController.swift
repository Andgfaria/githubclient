/* AboutViewController - classe criada por André Gimenez Faria em janeiro de 2016
   Classe com os dados de contato do desenvolvedor
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet var developerImageView: UIImageView!
    
    @IBOutlet var aboutTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        developerImageView.layer.cornerRadius = 64
        developerImageView.layer.masksToBounds = true
    }

    @IBAction func exit(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
