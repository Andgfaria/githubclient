/* PullRequestsViewControllers - classe criada por André Gimenez Faria em janeiro de 2016
   Classe para listagem dos pulls requests de um determinado repositório
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import SafariServices
import SDWebImage

class PullRequestsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Variáveis
    
    // Repositório selecionado na lista principal
    var repository  : Repository! {
        didSet {
            self.title = repository.name
            if viewReady {
                updateScreen()
            }
        }
    }
    
    // Pull requests do repositório selecionado
    var pullRequests : [PullRequest]?
    
    // Flag para assegurar que mudanças de UI só ocorram após o carregamento da view
    var viewReady = false
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var errorMessageLabel: UILabel!
    
    @IBOutlet var tryAgainButton: UIButton!
    
    // MARK: Configuração e Atualização da Tela
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewReady = true
        self.title = nil
        self.tableView.estimatedRowHeight = 72
        self.tableView.rowHeight = UITableViewAutomaticDimension
        tryAgainButton.layer.cornerRadius = 5.0
        tryAgainButton.layer.masksToBounds = true
        tryAgainButton.addTarget(self, action: Selector("fetchPullRequests"), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func viewWillAppear(animated: Bool) {
        updateScreen()
    }
    
    /* Esse método atualiza a interface e os dados na tela sempre que um repositório é selecionado.
       Como é utilizada um UISplitViewController inicialmente nenhum repositório é exibido e, sendo assim, os elementos da UI são inicialmente ocultados.
    */
    func updateScreen() {
        if repository != nil {
            self.title = repository.name
            self.navigationItem.rightBarButtonItem?.enabled = repository.name != nil && repository.url != nil
            fetchPullRequests()
        }
        else {
            self.tableView.hidden = true
            self.activityIndicator.hidden = true
            self.errorMessageLabel.hidden = true
            self.tryAgainButton.hidden = true
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
    }
    
    //MARK: Carregamento dos Dados
    
    // Carregamento dos pull requests do repositório
    func fetchPullRequests() {
        self.activityIndicator.hidden = false
        self.errorMessageLabel.hidden = true
        self.tryAgainButton.hidden = true
        APIManager.getPullRequestsFromRepository(repository) { (result, pullRequests) in
            dispatch_async(dispatch_get_main_queue(), {
                self.activityIndicator.hidden = true
                if result == .Success {
                    self.pullRequests = pullRequests
                    self.tableView.reloadData()
                    self.tableView.hidden = false
                }
                else {
                    self.tryAgainButton.hidden = false
                    if result == .ServerError {
                        self.errorMessageLabel.text = "Erro de Rede"
                    }
                    else {
                        self.errorMessageLabel.text = result == .JsonError ? "Erro no JSON" : "Dados do Repositório Insuficientes"
                    }
                    self.errorMessageLabel.hidden = false
                }
            })
        }
    }
    
    //MARK: Implementação dos protocolos UITableViewDataSource e UITableViewDelegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests?.count ?? 0
    }
    
    /* Células utilizadas:
    Pull Request Cell
    .1: UIImageView para exibir o avatar do autor
    .2: UILabel para o nome do autor
    .3: UILabel para o título do Pull Request
    .4: UILabel para o corpo do Pull Request
    .5: UILabel para a data do Pull Request
    */
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("pullRequestCell", forIndexPath: indexPath)
        if let authorAvatarImageView = cell.viewWithTag(1) as? UIImageView {
            authorAvatarImageView.layer.cornerRadius = 18
            authorAvatarImageView.layer.masksToBounds = true
            if pullRequests![indexPath.row].author?.avatarURL != nil {
                authorAvatarImageView.sd_setImageWithURL(NSURL(string: pullRequests![indexPath.row].author!.avatarURL!), placeholderImage: UIImage(named: "genericProfile.png"))
            }
            if let authorNameLabel = cell.viewWithTag(2) as? UILabel {
                authorNameLabel.text = pullRequests![indexPath.row].author?.name ?? "-"
            }
            if let pullRequestTitleLabel = cell.viewWithTag(3) as? UILabel {
                pullRequestTitleLabel.text = pullRequests![indexPath.row].title ?? "-"
            }
            if let pullRequestBodyLabel = cell.viewWithTag(4) as? UILabel {
                pullRequestBodyLabel.text = pullRequests![indexPath.row].body ?? "-"
            }
            if let pullRequestDateLabel = cell.viewWithTag(5) as? UILabel {
                if pullRequests![indexPath.row].date != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
                    pullRequestDateLabel.text = dateFormatter.stringFromDate(pullRequests![indexPath.row].date!)
                }
                else {
                    pullRequestDateLabel.text = "-"
                }
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Caso a url do Pull Request não seja nula, é apresentado um Safari View Controller
        if pullRequests![indexPath.row].url != nil {
            let safariViewController = SFSafariViewController(URL: NSURL(string: pullRequests![indexPath.row].url!)!)
            safariViewController.modalPresentationStyle = UIModalPresentationStyle.FormSheet
            self.presentViewController(safariViewController, animated: true, completion: nil)
        }
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    // MARK: Outros
    
    // Método para compartilhar a URL do repositório selecionado
    @IBAction func shareRepository(sender: AnyObject) {
        let activityViewController = UIActivityViewController(activityItems: [repository.name!, repository.url!], applicationActivities: nil)
        activityViewController.popoverPresentationController?.barButtonItem = sender as? UIBarButtonItem
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().clearDisk()
    }
    
}
